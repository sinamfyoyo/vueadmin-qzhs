const XLSX = require('xlsx')
import FileSaver from 'file-saver'
import XLSXStyle from 'xlsx-style'


/**
 * el-table数据导出execl文件
 *
 * @param {*} id:el-table元素id
 * @param {*} fileName :导出文件名称
 * @returns
 */
function exportExecl(id, fileName) {
  let excelName = fileName || '导出表格.xlsx'
  var xlsxParam = { raw: true }
  let tables = document.getElementById(id)
  if (!tables) return
  tables = document.getElementById(id).cloneNode(true)
  // 移除固定列，防止重复生成表格
  if (tables.querySelector('.el-table__fixed') !== null) {
    tables.removeChild(tables.querySelector('.el-table__fixed'))
  }
  let wb = XLSX.utils.book_new()
  XLSX.utils.book_append_sheet(wb, XLSX.utils.table_to_sheet(tables), '1')
  setExlStyle(wb['Sheets']['1'])
  console.log(wb['Sheets']['1'])
  addRangeBorder(wb['Sheets']['1']['!merges'], wb['Sheets']['1'])
  var table_write = XLSXStyle.write(wb, {
    type: 'buffer'
  })
  try {
    FileSaver.saveAs(
      new Blob([table_write], { type: 'application/octet-stream' }),
      excelName
    )
  } catch (e) {
    // console.log(e, table_write)
  }
  return table_write

}

function addRangeBorder(range, ws) {
  let cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
  range.forEach(item => {
    let style = {
      s: {
        border: {
          top: { style: 'thin' },
          left: { style: 'thin' },
          bottom: { style: 'thin' },
          right: { style: 'thin' }
        }
      }
    }
    // 处理合并行
    for (let i = item.s.c; i <= item.e.c; i++) {
      ws[`${cols[i]}${Number(item.e.r) + 1}`] = ws[`${cols[i]}${Number(item.e.r) + 1}`] || style
      // 处理合并列
      for (let k = item.s.r + 2; k <= item.e.r + 1; k++) {
        ws[cols[i] + k] = ws[cols[k] + item.e.r] || style
      }
    }
  })
  return ws
}

function setExlStyle(data) {
  let borderAll = {
    top: {
      style: 'thin'
    },
    bottom: {
      style: 'thin'
    },
    left: {
      style: 'thin'
    },
    right: {
      style: 'thin'
    }
  }
  data['!cols'] = []

  for (let key in data) {
    // console.log(key)
    if (data[key] instanceof Object) {
      if (key == 'A1') {
        data[key].s = {
          border: borderAll,
          alignment: {
            horizontal: 'center',
            vertical: 'center'
          },
          font: {
            sz: 16,
            color: { rgb: '202f3f' },
            bold:true
          },
          bold: true,
          numFmt: 0,
          hpx: 70,
          fill: {
            fgColor: { rgb: 'd9e4ef' }
          }
        }
      } else {
        data[key].s = {
          border: borderAll,
          alignment: {
            horizontal: 'center',
            vertical: 'center'
          },
          font: {
            sz: 11
          },
          bold: true,
          numFmt: 0
        }
      }

      data['!rows'].push({ hpx: 30 })
      data['!cols'].push({ wpx: 150 })
    }
  }
  for (let keys in data['!rows']) {
    if(keys==0){
      data['!rows'][keys].hpx=60
    }
  }
  console.log(data['!rows'])
  return data
}

export default exportExecl
