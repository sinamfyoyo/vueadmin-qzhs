import request from '@/utils/request'

export function getCountType() {
  return request({
    url: '/AdminJsonApi/getCountType',
    method: 'get'
  })
}

export function getRankData(data) {
  return request({
    url: '/AdminRankJsonApi/getRankData',
    method: 'post',
    data
  })
}
export function saveRankAreaData(data) {
  return request({
    url: '/AdminRankJsonApi/saveRankAreaData',
    method: 'post',
    data
  })
}
export function getReply(data) {
  return request({
    url: 'superAdmin.Reply/getReply',
    method: 'post',
    data
  })
}

export function getLingyuData(data) {
  return request({
    url: '/AdminRankJsonApi/getLingyuData',
    method: 'post',
    data
  })
}
export function getRankDom(data) {
  return request({
    url: '/AdminRankJsonApi/getRankDom',
    method: 'post',
    data
  })
}

export function getRankJigouData(data) {
  return request({
    url: '/AdminRankJsonApi/getRankJigouData',
    method: 'post',
    data
  })
}
