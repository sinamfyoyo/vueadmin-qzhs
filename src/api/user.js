


import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/AdminJsonApi/userlogin',
    method: 'post',
    data
  })
}

export function getInfo(data) {
  return request({
    url: '/AdminJsonApi/user_info',
    method: 'post',
    data:{token:data}
  })
}

export function getList(params) {
  return request({
    url: '/AdminJsonApi/user_list',
    method: 'get',
    params:params
  })
}
export function getMalaApi(params) {
  return request({
    url: '/AdminJsonApi/getMalaApi',
    method: 'post',
    params
  })
}
export function getadminList(params) {
  return request({
    url: '/AdminJsonApi/getadminList',
    method: 'post',
    params
  })
}
export function create_admin(params) {
  return request({
    url: '/AdminJsonApi/create_admin',
    method: 'post',
    data:params
  })
}

export function logout() {
  return request({
    url: '/AdminJsonApi/logout',
    method: 'post'
  })
}
export function getCity(data) {
  return request({
    url: '/AdminJsonApi/getCity',
    method: 'post',
    data
  })
}
export function createCity(data) {
  return request({
    url: '/AdminJsonApi/createCity',
    method: 'post',
    data
  })
}
export function updateUser(data) {
  return request({
    url: '/AdminJsonApi/updateUser',
    method: 'post',
    data
  })
}

export function restpwd(data) {
  return request({
    url: '/AdminJsonApi/restpwd',
    method: 'post',
    data
  })
}

export function getPhone(data) {
  return request({
    url: '/AdminJsonApi/getPhone',
    method: 'post',
    data
  })
}
export function rePwd(data) {
  return request({
    url: '/AdminJsonApi/rePwd',
    method: 'post',
    data
  })
}
export function getadminlog(params) {
  return request({
    url: '/AdminJsonApi/getadminlog',
    method: 'get',
    params
  })
}
export function getVerifyList(data) {
  return request({
    url: '/AdminJsonApi/getVerifyList',
    method: 'post',
    data
  })
}
export function actionVerify(data) {
  return request({
    url: '/AdminJsonApi/actionVerify',
    method: 'post',
    data
  })
}

export function getGeet(data) {
  return request({
    url: 'superAdmin.Login/auth',
    method: 'post',
    data
  })
}
export function sendCode(data) {
  return request({
    url: 'superAdmin.User/sendcode',
    method: 'post',
    data
  })
}
export function bindCode(data) {
  return request({
    url: 'superAdmin.User/bindPhone',
    method: 'post',
    data
  })
}

export function getMsg() {
  return request({
    url: 'superAdmin.Message/getUnread',
    method: 'get',
  })
}
export function getMyMsgList() {
  return request({
    url: 'superAdmin.Message/getMyMsgList',
    method: 'post',
  })
}
export function goReadMsg(data) {
  return request({
    url: 'superAdmin.Message/goReadMsg',
    method: 'post',
    data
  })
}

export function getALLadmincp() {
  return request({
    url: 'superAdmin.Message/getALLadmincp',
    method: 'get',
  })
}
export function doSendMessage(data) {
  return request({
    url: 'superAdmin.Message/doSendMessage',
    method: 'post',
    data
  })
}
export function authUserLogin(data) {
  return request({
    url: 'superAdmin.User/authUserLogin',
    method: 'post',
    data
  })
}
