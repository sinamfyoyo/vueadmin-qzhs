
import request from '@/utils/request'

export function getBaseData(data) {
  return request({
    url: 'superAdmin.Count/getBaseData',
    method: 'post',
    data
  })
}
export function uploadHb(data) {
  return request({
    url: 'superAdmin.Count/douploadHb',
    method: 'post',
    data
  })
}

export function searchSCK(data) {
  return request({
    url: 'superAdmin.Count/searchSCK',
    method: 'post',
    data
  })
}
export function DeleteSucai(data) {
  return request({
    url: 'superAdmin.Report/DeleteSucai',
    method: 'post',
    data
  })
}
export function getWord() {
  return request({
    url: 'bigData.CountData/getWord',
    method: 'get'
  })
}
export function getLingyu(data) {
  return request({
    url: 'superAdmin.Count/getLingyu',
    method: 'post',
    data
  })
}
export function getGuanjianci(data) {
  return request({
    url: 'superAdmin.Count/getGuanjianci',
    method: 'post',
    data
  })
}
export function getShengzhi(data) {
  return request({
    url: 'superAdmin.Count/getShengzhi',
    method: 'post',
    data
  })
}
export function getShizhou(data) {
  return request({
    url: 'superAdmin.Count/getShizhou',
    method: 'post',
    data
  })
}
export function getXianshi(data) {
  return request({
    url: 'superAdmin.Count/getXianshi',
    method: 'post',
    data
  })
}
