import request from '@/utils/request'

export function getDoubaoSimpleRibao(data) {
  return request({
    url: 'superAdmin.Ai/RibaoSimple',
    method: 'post',
    data
  })
}
