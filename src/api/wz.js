
import request from '@/utils/request'

export function getWzParams() {
  return request({
    url: 'AdminJsonApi/getWzParams',
    method: 'get'
  })
}
export function getAllly() {
  return request({
    url: 'AdminJsonApi/getAllly',
    method: 'get'
  })
}
export function getdiyclassify(data) {
  return request({
    url: 'AdminJsonApi/getdiyclassify',
    method: 'post',
    data
  })
}

export function delImg(data) {
  return request({
    url: 'AdminJsonApi/delImg',
    method: 'post',
    data
  })
}
export function getUploadReply(data) {
  return request({
    url: 'AdminJsonApi/getUploadReply',
    method: 'post',
    data
  })
}
export function getUploadReplyLog(data) {
  return request({
    url: 'AdminJsonApi/getUploadReplyLog',
    method: 'post',
    data
  })
}

export function getWzlist(data) {
  return request({
    url: 'AdminJsonApi/Wzlist',
    method: 'post',
    data
  })
}

export function getWzlistexcel(data) {
  return request({
    url: 'AdminJsonApi/Wzlist',
    method: 'post',
    responseType: 'blob',
    data
  })
}
export function getCQWzlist(data) {
  return request({
    url: 'AdminJsonApi/getCQWzlist',
    method: 'post',
    data
  })
}
export function getWzinfo(data) {
  return request({
    url: 'AdminJsonApi/getWzinfo',
    method: 'post',
    data
  })
}
export function getCityAll(data) {
  return request({
    url: 'AdminJsonApi/getCityAll',
    method: 'post',
    data
  })
}
export function getShensuList(data) {
  return request({
    url: 'AdminJsonApi/getShensuList',
    method: 'post',
    data
  })
}
export function shensuBack(data) {
  return request({
    url: 'AdminJsonApi/shensuBack',
    method: 'post',
    data
  })
}
export function updateTag(data) {
  return request({
    url: 'AdminJsonApi/updateTag',
    method: 'post',
    data
  })
}
export function updateWzinfo(data) {
  return request({
    url: 'AdminJsonApi/updateWzinfo',
    method: 'post',
    data
  })
}
export function updateWzinfoParms(data) {
  return request({
    url: 'AdminJsonApi/updateWzinfoParms',
    method: 'post',
    data
  })
}
export function updateWzinfoEndtime(data) {
  return request({
    url: 'AdminJsonApi/updateWzinfoEndtime',
    method: 'post',
    data
  })
}
export function getRibaoWzinfo(data) {
  return request({
    url: 'AdminJsonApi/getWzinfo',
    method: 'post',
    data
  })
}

export function delWzinfo(data) {
  return request({
    url: 'AdminJsonApi/delWzinfo',
    method: 'post',
    data
  })
}

export function getDayReport(data) {
  return request({
    url: 'superAdmin.Report/getDayReport',
    method: 'post',
    data
  })
}


export function Aicreate(data) {
  return request({
    url: 'superAdmin.Report/Aicreate',
    method: 'post',
    data
  })
}
export function reportDopost(data) {
  return request({
    url: 'superAdmin.Report/reportDopost',
    method: 'post',
    data
  })
}
export function getWeekReport(data) {
  return request({
    url: 'superAdmin.Report/getWeekReport',
    method: 'post',
    data
  })
}
export function getMonthReport(data) {
  return request({
    url: 'superAdmin.Report/getMonthReport',
    method: 'post',
    data
  })
}
export function getForm(data) {
  return request({
    url: 'userAdmin.Form/getForm',
    method: 'post',
    data
  })
}
export function getFormData(data) {
  return request({
    url: 'userAdmin.Form/getFormData',
    method: 'post',
    data
  })
}
export function getFastReply(data) {
  return request({
    url: 'superAdmin.Report/getFastReply',
    method: 'post',
    data
  })
}
export function addFastReply(data) {
  return request({
    url: 'superAdmin.Report/addFastReply',
    method: 'post',
    data
  })
}
export function getByIdFastReply(data) {
  return request({
    url: 'superAdmin.Report/getByIdFastReply',
    method: 'post',
    data
  })
}
export function delFastReply(data) {
  return request({
    url: 'superAdmin.Report/delFastReply',
    method: 'post',
    data
  })
}

export function AiWeekcreate(data) {
  return request({
    url: 'superAdmin.Report/AiWeekcreate',
    method: 'post',
    data
  })
}
export function AiMonthcreate(data) {
  return request({
    url: 'superAdmin.Count/Aitext',
    method: 'post',
    data
  })
}
export function reportDopostYuebao(data) {
  return request({
    url: 'superAdmin.Report/reportDopostYuebao',
    method: 'post',
    data
  })
}
export function getGeomap() {
  return request({
    url: 'https://geo.datav.aliyun.com/areas_v3/bound/510000_full.json',
    method: 'get'
  })
}

export function saveDiySpecial(data) {
  return request({
    url: 'AdminJsonApi/saveDiySpecial',
    method: 'post',
    data
  })
}
export function updateDiySpecial(data) {
  return request({
    url: 'AdminJsonApi/updateDiySpecial',
    method: 'post',
    data
  })
}
export function getDiy(data) {
  return request({
    url: 'AdminJsonApi/getDiy',
    method: 'post',
    data
  })
}
